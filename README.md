# Introdução

   TB6612FNG é um Driver de motor de corrente contínua,cuja a função é o controle de velocidade, sentido de rotação e frenagem dos motores conectados a eles. Cada motor pode ser controlado independente do outro.

   O driver pode controlar até dois motores simultâneamente em uma corrente de 1.2/3.2A, com dois sinais de entrada (IN1 e IN2) pode ser usado para controlar o motor em um dos quatro modos de função CW, CCW, curto-freio, e parar.

## Algumas de suas características são:
- tensão de alimentação: VM = 15 V max, VCC = 2.7-5.5 V
- corrente de saída: 1.2A Iout = (média)/3.2A (pico)
- controle de espera para economizar energia
- CW/CCW/short freio/stop modos de controle do motor
- circuito de desligamento térmico e circuito de detecção de baixa tensão
- capacitores de filtragem em ambas as linhas de abastecimento
dimensões: 2 cm x 2cm
 

![](https://gitlab.com/eguinaldocouras/diretrizes-de-funcionamento-sensor-tb6612fng/-/raw/main/Image/ENTRADAS%20E%20SAIDAS.png)

# Entradas e Saídas

   <details>
   <Summary><b>GND<b></Summary>
   <br>

   **Nome**

   GROUND.

   **Tipo**

   Pólo negativo.

   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**

   Todas deverão ser devidamente conectadas em entradas GNDS do arduino ou da placa de circuito usada.

   **Descrição**

   Entradas GNDs padrão.
   </details> 

---

   <details>
   <Summary><b>VM<b></Summary>
   <br>

   **Nome**

   Motor voltage.

   **Tipo**

   Pólo positivo - até 15v.

   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**

   Pode ser conectada em uma conexão VCC do arduino, ou diretamente em uma bareria de até 15v(recomendado).

   **Descrição**

   VM É a entrada que alimentará os motores com a tensão usada .

   </details> 
  

---

   <details>
   <Summary><b>VCC<b></Summary>
   <br>

   **Nome**
   
   Logic Voltage.

   **Tipo**

   Pólo positivo - 2.5v até 5.5v.

   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**

   Pode ser alimentada pela entrada vcc do arduino..

   **Descrição**

   VCC é a entrada que ativará o driver.

   </details>
  

---

   <details>
   <Summary><b>STDBY<b></Summary>
   <br>

   **Nome**
   
   Stand by.

   **Tipo**

   Porta digital - 2.5v.

   **Comando de ativação**

   ```
   digitalWrite(STD, HIGH);
   ```
   **Modo de conexão**

   Deve estar conectada a uma porta digital do arduino.

   **Descrição**

   Sua funcionalidade é manter o driver ativo ou não, caso este pino esteja desligado o driver não 
   funcionará.

   </details>

---

   <details>
   <Summary><b>A01 e A02 / B01 e B02<b></Summary>

   **Tipo**

   Alimentação dos motores.

   **Comando de ativaão**

   ```
   Não é necessário.
   ```
   **Modo de conexção**

   Deve ser conectado nas entradas dos motores como demonstra a imagem abaixo. <br>
   ![](https://gitlab.com/eguinaldocouras/diretrizes-de-funcionamento-sensor-tb6612fng/-/raw/main/Image/a01%20a02.png)<br>

   **Descrição**
   
   Essas entradas são responsáveis por controlar a velocidade e rotação dos motores por meio das funçoes CW, CCW, curto freio, parar, que logo serão mais detalhadas. O mesmo acontece com as entradas B01 e B02

   </details>


---

   <details>
   <Summary><b>AIN1 e AIN2 / BIN1 e BIN2<b></Summary>
   <br>

   **Tipo**

   Porta digital - 2.5v.

   **Comando de ativação**

   ```
   pinMode(ain_1, OUTPUT);
   pinMode(ain_2, OUTPUT);
   pinMode(dirHorario, OUTPUT);

void sentidoMotores(int dirHorario){
   // Caso o pino esteja ativo o motor irá rotacionar em sentido horário
   if(dirHorario = 1) 
   {
      digitalWrite(ain_1, LOW);
      digitalWrite(ain_2, HIGH);
   }
   // Caso o pino esteja desligado o motor irá rotacionar em sentido antihorário
   else
   {
      digitalWrite(ain_1, HIGH);
      digitalWrite(ain_2, LOW);
   }
}
   ```
   **Modo de conexão**

   Deve estar conectada a uma porta digital-PWM do arduino.

   **Descrição**

   Essas duas portas serão responsáveis por controlar o sentido de rotação dos motores. Para alternar o sentido de rotação do motor, basta ligar uma porta e desligar a outra

   **ATENÇÃO** -> o sentido de rotação pode ser alterado, dependendo da maneira na qual as ligações das portas a01 e a02 foram feitas nos motores.
   **obs**: o mesmo vale para as portas BIN1 e BIN2, porém elas controlarão a rotação do motor conectado as portas B01 e B02.<br>
   </details>

---

   <details>
   <Summary><b>PWMB e PWMA<b></Summary>
   <br>

   **Tipo**

   Porta analógica.

   **Comando de ativação**

   ```
   analogWrite(PWM, valor);
   ```
   **Modo de conexão**

   Deve estar conectada a uma porta digital do arduino.

   **Descrição**

   Essas duas entradas controlarão a velocidade dos motores.Para definirmos a velocidade devemos utilizar utilizar o **comando de ativação**, e o valor deverá estar entre 0 e 255, em que 0 é a mininma potência e 255 é a máxima potência.

   </details>

---

# Funções

   
   <details>
   <Summary><b>CW<b></Summary>
   <br>

   **Nome**


   **Tipo**


   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**


   **Descrição**

   Entradas GNDs padrão.
   </details>

   ---

   <details>
   <Summary><b>CCW<b></Summary>
   <br>

   **Nome**


   **Tipo**


   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**


   **Descrição**

   Entradas GNDs padrão.
   </details>

   ---

   <details>
   <Summary><b>FREIO<b></Summary>
   <br>

   **Nome**


   **Tipo**


   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**


   **Descrição**

   Entradas GNDs padrão.
   </details>

   ---

   <details>
   <Summary><b>Curto freio<b></Summary>
   <br>

   **Nome**


   **Tipo**


   **Comando de ativação**

   ```
   Não é necessário
   ```
   **Modo de conexão**


   **Descrição**

   Entradas GNDs padrão.
   </details>






